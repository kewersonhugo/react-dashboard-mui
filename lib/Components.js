(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("react-dashboard-mui", [], factory);
	else if(typeof exports === 'object')
		exports["react-dashboard-mui"] = factory();
	else
		root["react-dashboard-mui"] = factory();
})(typeof self !== 'undefined' ? self : this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 93);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var NAMESPACE = 'REACT_DASHBOARD_MUI_';

var actions = {
  TOGGLE_SIDE_MENU: NAMESPACE + 'TOGGLE_SIDE_MENU',
  UPDATED_FORM: NAMESPACE + 'UPDATED_FORM',
  REGISTER_NEW_USER: NAMESPACE + 'REGISTER_NEW_USER',
  REGISTER_NEW_USER_SUCCESSFUL: NAMESPACE + 'REGISTER_NEW_USER_SUCCESSFUL',
  REGISTER_NEW_USER_FAILED: NAMESPACE + 'REGISTER_NEW_USER_FAILED',
  RECOVER_USER_PASSWORD: NAMESPACE + 'RECOVER_USER_PASSWORD',
  RECOVER_USER_PASSWORD_SUCCESSFUL: NAMESPACE + 'RECOVER_USER_PASSWORD_SUCCESSFUL',
  RECOVER_USER_PASSWORD_FAILED: NAMESPACE + 'RECOVER_USER_PASSWORD_FAILED',
  LOGIN_USER: NAMESPACE + 'LOGIN_USER',
  LOGIN_USER_WITH_FACEBOOK: NAMESPACE + 'LOGIN_USER_WITH_FACEBOOK',
  LOGIN_USER_WITH_GOOGLE: NAMESPACE + 'LOGIN_USER_WITH_GOOGLE',
  LOGIN_USER_SUCCESSFUL: NAMESPACE + 'LOGIN_USER_SUCCESSFUL',
  LOGIN_USER_FAILED: NAMESPACE + 'LOGIN_USER_FAILED',
  LOGOUT_USER: NAMESPACE + 'LOGOUT_USER'
};

exports.default = actions;

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),
/* 4 */
/***/ (function(module, exports) {

var core = module.exports = { version: '2.5.3' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(8)(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("material-ui/styles");

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(3);
var core = __webpack_require__(4);
var ctx = __webpack_require__(41);
var hide = __webpack_require__(13);
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && key in exports) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__(14);
var IE8_DOM_DEFINE = __webpack_require__(42);
var toPrimitive = __webpack_require__(25);
var dP = Object.defineProperty;

exports.f = __webpack_require__(5) ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),
/* 11 */
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__(26);
var defined = __webpack_require__(18);
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(10);
var createDesc = __webpack_require__(16);
module.exports = __webpack_require__(5) ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(7);
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__(44);
var enumBugKeys = __webpack_require__(29);

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),
/* 18 */
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),
/* 19 */
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),
/* 20 */
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),
/* 21 */
/***/ (function(module, exports) {

exports.f = {}.propertyIsEnumerable;


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__(28)('wks');
var uid = __webpack_require__(20);
var Symbol = __webpack_require__(3).Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),
/* 23 */,
/* 24 */,
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(7);
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__(45);
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__(28)('keys');
var uid = __webpack_require__(20);
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(3);
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});
module.exports = function (key) {
  return store[key] || (store[key] = {});
};


/***/ }),
/* 29 */
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),
/* 30 */
/***/ (function(module, exports) {

exports.f = Object.getOwnPropertySymbols;


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__(18);
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var VALID_EMAIL_REGEX = exports.VALID_EMAIL_REGEX = new RegExp(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

exports.default = {
  VALID_EMAIL_REGEX: VALID_EMAIL_REGEX
};

/***/ }),
/* 33 */,
/* 34 */,
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(95), __esModule: true };

/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(67);

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof2 = __webpack_require__(68);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && ((typeof call === "undefined" ? "undefined" : (0, _typeof3.default)(call)) === "object" || typeof call === "function") ? call : self;
};

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setPrototypeOf = __webpack_require__(121);

var _setPrototypeOf2 = _interopRequireDefault(_setPrototypeOf);

var _create = __webpack_require__(125);

var _create2 = _interopRequireDefault(_create);

var _typeof2 = __webpack_require__(68);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : (0, _typeof3.default)(superClass)));
  }

  subClass.prototype = (0, _create2.default)(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf2.default ? (0, _setPrototypeOf2.default)(subClass, superClass) : subClass.__proto__ = superClass;
};

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _assign = __webpack_require__(46);

var _assign2 = _interopRequireDefault(_assign);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _assign2.default || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__(49);
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__(5) && !__webpack_require__(8)(function () {
  return Object.defineProperty(__webpack_require__(43)('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(7);
var document = __webpack_require__(3).document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__(11);
var toIObject = __webpack_require__(12);
var arrayIndexOf = __webpack_require__(51)(false);
var IE_PROTO = __webpack_require__(27)('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),
/* 45 */
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(47), __esModule: true };

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(48);
module.exports = __webpack_require__(4).Object.assign;


/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.1 Object.assign(target, source)
var $export = __webpack_require__(9);

$export($export.S + $export.F, 'Object', { assign: __webpack_require__(50) });


/***/ }),
/* 49 */
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// 19.1.2.1 Object.assign(target, source, ...)
var getKeys = __webpack_require__(17);
var gOPS = __webpack_require__(30);
var pIE = __webpack_require__(21);
var toObject = __webpack_require__(31);
var IObject = __webpack_require__(26);
var $assign = Object.assign;

// should work with symbols and should have deterministic property order (V8 bug)
module.exports = !$assign || __webpack_require__(8)(function () {
  var A = {};
  var B = {};
  // eslint-disable-next-line no-undef
  var S = Symbol();
  var K = 'abcdefghijklmnopqrst';
  A[S] = 7;
  K.split('').forEach(function (k) { B[k] = k; });
  return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K;
}) ? function assign(target, source) { // eslint-disable-line no-unused-vars
  var T = toObject(target);
  var aLen = arguments.length;
  var index = 1;
  var getSymbols = gOPS.f;
  var isEnum = pIE.f;
  while (aLen > index) {
    var S = IObject(arguments[index++]);
    var keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S);
    var length = keys.length;
    var j = 0;
    var key;
    while (length > j) if (isEnum.call(S, key = keys[j++])) T[key] = S[key];
  } return T;
} : $assign;


/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__(12);
var toLength = __webpack_require__(52);
var toAbsoluteIndex = __webpack_require__(53);
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__(19);
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(19);
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),
/* 54 */
/***/ (function(module, exports) {

module.exports = true;


/***/ }),
/* 55 */
/***/ (function(module, exports) {

module.exports = {};


/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__(14);
var dPs = __webpack_require__(105);
var enumBugKeys = __webpack_require__(29);
var IE_PROTO = __webpack_require__(27)('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__(43)('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__(106).appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

var def = __webpack_require__(10).f;
var has = __webpack_require__(11);
var TAG = __webpack_require__(22)('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

exports.f = __webpack_require__(22);


/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(3);
var core = __webpack_require__(4);
var LIBRARY = __webpack_require__(54);
var wksExt = __webpack_require__(58);
var defineProperty = __webpack_require__(10).f;
module.exports = function (name) {
  var $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {});
  if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: wksExt.f(name) });
};


/***/ }),
/* 60 */
/***/ (function(module, exports) {

module.exports = require("material-ui/Paper");

/***/ }),
/* 61 */
/***/ (function(module, exports) {

module.exports = require("material-ui/TextField");

/***/ }),
/* 62 */
/***/ (function(module, exports) {

module.exports = require("material-ui/Button");

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var parseFirebaseAuthError = function parseFirebaseAuthError() {
  var error = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  switch (error.code) {
    case 'auth/weak-password':
      return {
        field: 'PASSWORD',
        message: 'Sua senha deve ter no mínimo 6 caracteres'
      };
    case 'auth/wrong-password':
      return {
        field: 'PASSWORD',
        message: 'Senha incorreta'
      };
    case 'auth/user-not-found':
      return {
        field: 'EMAIL',
        message: 'O usuário não existe ou foi deletado'
      };
    case 'auth/invalid-email':
      return {
        field: 'EMAIL',
        message: 'Insira um endereço de e-mail válido'
      };
    case 'auth/email-already-in-use':
      return {
        field: 'EMAIL',
        message: 'Este e-mail já está cadastrado'
      };
    default:
      return {};
  }
};

exports.default = parseFirebaseAuthError;

/***/ }),
/* 64 */,
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(35);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(36);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(37);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(38);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(39);

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(2);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = __webpack_require__(6);

var _classnames = __webpack_require__(73);

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var drawerWidth = 240;

var styles = function styles() {
  return {
    menuOpened: {
      '&$root': {
        marginLeft: drawerWidth + 'px',
        width: 'calc(100% - ' + drawerWidth + 'px)'
      }
    },
    root: {
      position: 'absolute',
      width: '100%',
      height: '100%'
    }
  };
};

var Dashboard = function (_React$PureComponent) {
  (0, _inherits3.default)(Dashboard, _React$PureComponent);

  function Dashboard() {
    (0, _classCallCheck3.default)(this, Dashboard);
    return (0, _possibleConstructorReturn3.default)(this, (Dashboard.__proto__ || (0, _getPrototypeOf2.default)(Dashboard)).apply(this, arguments));
  }

  (0, _createClass3.default)(Dashboard, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          children = _props.children,
          open = _props.open;

      return _react2.default.createElement(
        'div',
        { className: (0, _classnames2.default)(classes.root, open && classes.menuOpened) },
        children
      );
    }
  }]);
  return Dashboard;
}(_react2.default.PureComponent);

Dashboard.defaultProps = {
  children: null,
  open: false
};

Dashboard.propTypes = {
  children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]),
  classes: _propTypes2.default.shape({
    root: _propTypes2.default.string,
    appBar: _propTypes2.default.string
  }).isRequired,
  open: _propTypes2.default.bool
};

exports.default = (0, _styles.withStyles)(styles)(Dashboard);

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__(11);
var toObject = __webpack_require__(31);
var IE_PROTO = __webpack_require__(27)('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(98), __esModule: true };

/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _iterator = __webpack_require__(100);

var _iterator2 = _interopRequireDefault(_iterator);

var _symbol = __webpack_require__(111);

var _symbol2 = _interopRequireDefault(_symbol);

var _typeof = typeof _symbol2.default === "function" && typeof _iterator2.default === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = typeof _symbol2.default === "function" && _typeof(_iterator2.default) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof(obj);
} : function (obj) {
  return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof(obj);
};

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var LIBRARY = __webpack_require__(54);
var $export = __webpack_require__(9);
var redefine = __webpack_require__(70);
var hide = __webpack_require__(13);
var has = __webpack_require__(11);
var Iterators = __webpack_require__(55);
var $iterCreate = __webpack_require__(104);
var setToStringTag = __webpack_require__(57);
var getPrototypeOf = __webpack_require__(66);
var ITERATOR = __webpack_require__(22)('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = (!BUGGY && $native) || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && !has(IteratorPrototype, ITERATOR)) hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(13);


/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
var $keys = __webpack_require__(44);
var hiddenKeys = __webpack_require__(29).concat('length', 'prototype');

exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return $keys(O, hiddenKeys);
};


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

var pIE = __webpack_require__(21);
var createDesc = __webpack_require__(16);
var toIObject = __webpack_require__(12);
var toPrimitive = __webpack_require__(25);
var has = __webpack_require__(11);
var IE8_DOM_DEFINE = __webpack_require__(42);
var gOPD = Object.getOwnPropertyDescriptor;

exports.f = __webpack_require__(5) ? gOPD : function getOwnPropertyDescriptor(O, P) {
  O = toIObject(O);
  P = toPrimitive(P, true);
  if (IE8_DOM_DEFINE) try {
    return gOPD(O, P);
  } catch (e) { /* empty */ }
  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
};


/***/ }),
/* 73 */
/***/ (function(module, exports) {

module.exports = require("classnames");

/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(2);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Drawer = __webpack_require__(134);

var _Drawer2 = _interopRequireDefault(_Drawer);

var _styles = __webpack_require__(6);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var drawerWidth = 240;

var styles = function styles() {
  return {
    drawerPaper: {
      position: 'fixed',
      height: '100%',
      width: drawerWidth
    }
  };
};

var SideMenu = function SideMenu(_ref) {
  var children = _ref.children,
      classes = _ref.classes,
      open = _ref.open;
  return _react2.default.createElement(
    _Drawer2.default,
    {
      classes: {
        paper: classes.drawerPaper
      },
      type: 'persistent',
      open: open
    },
    children
  );
};

SideMenu.defaultProps = {
  children: null,
  open: false
};

SideMenu.propTypes = {
  children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]),
  classes: _propTypes2.default.shape({
    drawerPaper: _propTypes2.default.string
  }).isRequired,
  open: _propTypes2.default.bool
};

exports.default = (0, _styles.withStyles)(styles)(SideMenu);

/***/ }),
/* 75 */
/***/ (function(module, exports) {

module.exports = require("material-ui/IconButton");

/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(35);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(36);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(37);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(38);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(39);

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(2);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Paper = __webpack_require__(60);

var _Paper2 = _interopRequireDefault(_Paper);

var _TextField = __webpack_require__(61);

var _TextField2 = _interopRequireDefault(_TextField);

var _Button = __webpack_require__(62);

var _Button2 = _interopRequireDefault(_Button);

var _styles = __webpack_require__(6);

var _regex = __webpack_require__(32);

var _utils = __webpack_require__(63);

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    root: {
      position: 'absolute',
      width: '100%',
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
    },
    buttonWrapper: {
      display: 'flex',
      textAlign: 'center',
      marginTop: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit * 2,
      '& > button, > a': {
        margin: '8px'
      }
    },
    facebook: {
      backgroundColor: '#3b5998',
      color: 'white'
    },
    google: {
      backgroundColor: '#d34836',
      color: 'white'
    },
    paper: {
      padding: '32px'
    },
    form: {
      width: 'auto',
      maxWidth: '400px'
    },
    confirmButton: {
      width: '100%',
      marginTop: theme.spacing.unit
    }
  };
};

var Login = function (_React$PureComponent) {
  (0, _inherits3.default)(Login, _React$PureComponent);

  function Login() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Login);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Login.__proto__ || (0, _getPrototypeOf2.default)(Login)).call.apply(_ref, [this].concat(args))), _this), _this.onEmailChange = function (event) {
      return _this.props.onChange({
        email: event.target.value
      });
    }, _this.onPasswordChange = function (event) {
      return _this.props.onChange({
        password: event.target.value
      });
    }, _this.onSubmit = function (event) {
      event.preventDefault();
      var _this$props$user = _this.props.user,
          email = _this$props$user.email,
          password = _this$props$user.password;

      _this.props.loginUser(email, password);
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Login, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          error = _props.user.error;

      var parsedAuthError = (0, _utils2.default)(error);
      return _react2.default.createElement(
        'div',
        { className: classes.root },
        _react2.default.createElement(
          'div',
          { className: classes.buttonWrapper },
          _react2.default.createElement(
            _Button2.default,
            {
              className: classes.facebook,
              onClick: this.props.loginWithFacebook
            },
            'Login com Facebook'
          ),
          _react2.default.createElement(
            _Button2.default,
            {
              className: classes.google,
              onClick: this.props.loginWithGoogle
            },
            'Login com Google'
          )
        ),
        _react2.default.createElement(
          _Paper2.default,
          { className: classes.paper },
          _react2.default.createElement(
            'form',
            {
              className: classes.form,
              onSubmit: this.onSubmit
            },
            _react2.default.createElement(_TextField2.default, {
              margin: 'dense',
              label: 'E-mail',
              fullWidth: true,
              required: true,
              error: parsedAuthError.field === 'EMAIL',
              helperText: parsedAuthError.message,
              InputProps: {
                inputProps: {
                  pattern: _regex.VALID_EMAIL_REGEX.source,
                  title: 'Insira um e-mail válido'
                }
              },
              onChange: this.onEmailChange
            }),
            _react2.default.createElement(_TextField2.default, {
              margin: 'dense',
              type: 'password',
              label: 'Password',
              fullWidth: true,
              required: true,
              error: parsedAuthError.field === 'PASSWORD',
              helperText: parsedAuthError.message,
              onChange: this.onPasswordChange
            }),
            _react2.default.createElement(
              'div',
              null,
              _react2.default.createElement(
                _Button2.default,
                {
                  className: classes.confirmButton,
                  type: 'submit',
                  color: 'accent',
                  raised: true
                },
                'Login'
              )
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: classes.buttonWrapper },
          _react2.default.createElement(
            _Button2.default,
            { href: '/recover-password' },
            'Esqueci minha senha'
          ),
          _react2.default.createElement(
            _Button2.default,
            { href: '/register' },
            'Cadastrar'
          )
        )
      );
    }
  }]);
  return Login;
}(_react2.default.PureComponent);

Login.propTypes = {
  classes: _propTypes2.default.shape({
    root: _propTypes2.default.string
  }).isRequired,
  user: _propTypes2.default.shape({
    email: _propTypes2.default.string,
    password: _propTypes2.default.string,
    error: _propTypes2.default.object
  }).isRequired,
  onChange: _propTypes2.default.func.isRequired,
  loginUser: _propTypes2.default.func.isRequired,
  loginWithFacebook: _propTypes2.default.func.isRequired,
  loginWithGoogle: _propTypes2.default.func.isRequired
};

exports.default = (0, _styles.withStyles)(styles)(Login);

/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(35);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(36);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(37);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(38);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(39);

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(2);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Paper = __webpack_require__(60);

var _Paper2 = _interopRequireDefault(_Paper);

var _TextField = __webpack_require__(61);

var _TextField2 = _interopRequireDefault(_TextField);

var _Button = __webpack_require__(62);

var _Button2 = _interopRequireDefault(_Button);

var _styles = __webpack_require__(6);

var _regex = __webpack_require__(32);

var _utils = __webpack_require__(63);

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    root: {
      position: 'absolute',
      width: '100%',
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
    },
    paper: {
      padding: '32px'
    },
    form: {
      width: 'auto',
      maxWidth: '400px'
    },
    confirmButton: {
      width: '100%',
      marginTop: theme.spacing.unit
    },
    buttonWrapper: {
      marginTop: theme.spacing.unit * 2
    }
  };
};

var RegisterForm = function (_React$PureComponent) {
  (0, _inherits3.default)(RegisterForm, _React$PureComponent);

  function RegisterForm() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, RegisterForm);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = RegisterForm.__proto__ || (0, _getPrototypeOf2.default)(RegisterForm)).call.apply(_ref, [this].concat(args))), _this), _this.onEmailChange = function (event) {
      return _this.props.onChange({
        email: event.target.value
      });
    }, _this.onPasswordChange = function (event) {
      return _this.props.onChange({
        password: event.target.value
      });
    }, _this.onConfirmPasswordChange = function (event) {
      return _this.props.onChange({
        confirmPassword: event.target.value
      });
    }, _this.onSubmit = function (event) {
      event.preventDefault();
      var _this$props$user = _this.props.user,
          email = _this$props$user.email,
          password = _this$props$user.password,
          confirmPassword = _this$props$user.confirmPassword;

      if (confirmPassword === password) _this.props.registerNewUser(email, password);
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(RegisterForm, [{
    key: 'render',
    value: function render() {
      var classes = this.props.classes;
      var _props$user = this.props.user,
          password = _props$user.password,
          confirmPassword = _props$user.confirmPassword,
          error = _props$user.error;

      var parsedAuthError = (0, _utils2.default)(error);
      return _react2.default.createElement(
        'div',
        { className: classes.root },
        _react2.default.createElement(
          _Paper2.default,
          { className: classes.paper },
          _react2.default.createElement(
            'form',
            {
              className: classes.form,
              onSubmit: this.onSubmit
            },
            _react2.default.createElement(_TextField2.default, {
              margin: 'dense',
              type: 'text',
              label: 'E-mail',
              fullWidth: true,
              required: true,
              error: parsedAuthError.field === 'EMAIL',
              helperText: parsedAuthError.message,
              InputProps: {
                inputProps: {
                  pattern: _regex.VALID_EMAIL_REGEX.source,
                  title: 'Insira um e-mail válido'
                }
              },
              onChange: this.onEmailChange
            }),
            _react2.default.createElement(_TextField2.default, {
              margin: 'dense',
              label: 'Senha',
              fullWidth: true,
              required: true,
              error: parsedAuthError.field === 'PASSWORD',
              helperText: parsedAuthError.message,
              InputProps: {
                inputProps: {
                  type: 'password',
                  minLength: '6'
                }
              },
              onChange: this.onPasswordChange
            }),
            _react2.default.createElement(_TextField2.default, {
              margin: 'dense',
              type: 'password',
              label: 'Confirmar senha',
              fullWidth: true,
              required: true,
              error: confirmPassword !== password,
              onChange: this.onConfirmPasswordChange
            }),
            _react2.default.createElement(
              'div',
              null,
              _react2.default.createElement(
                _Button2.default,
                {
                  className: classes.confirmButton,
                  type: 'submit',
                  color: 'accent',
                  raised: true
                },
                'Cadastrar'
              )
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: classes.buttonWrapper },
          _react2.default.createElement(
            _Button2.default,
            { href: '/login' },
            'J\xE1 tenho uma conta'
          )
        )
      );
    }
  }]);
  return RegisterForm;
}(_react2.default.PureComponent);

RegisterForm.propTypes = {
  classes: _propTypes2.default.shape({
    root: _propTypes2.default.string
  }).isRequired,
  user: _propTypes2.default.shape({
    email: _propTypes2.default.string,
    password: _propTypes2.default.string,
    confirmPassword: _propTypes2.default.string,
    error: _propTypes2.default.object
  }).isRequired,
  onChange: _propTypes2.default.func.isRequired,
  registerNewUser: _propTypes2.default.func.isRequired
};

exports.default = (0, _styles.withStyles)(styles)(RegisterForm);

/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(35);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(36);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(37);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(38);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(39);

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(2);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Paper = __webpack_require__(60);

var _Paper2 = _interopRequireDefault(_Paper);

var _TextField = __webpack_require__(61);

var _TextField2 = _interopRequireDefault(_TextField);

var _Button = __webpack_require__(62);

var _Button2 = _interopRequireDefault(_Button);

var _styles = __webpack_require__(6);

var _regex = __webpack_require__(32);

var _utils = __webpack_require__(63);

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    root: {
      position: 'absolute',
      width: '100%',
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
    },
    paper: {
      padding: '32px'
    },
    form: {
      width: 'auto',
      maxWidth: '400px'
    },
    confirmButton: {
      width: '100%',
      marginTop: theme.spacing.unit
    },
    buttonWrapper: {
      marginTop: theme.spacing.unit * 2
    }
  };
};

var RecoverPasswordForm = function (_React$PureComponent) {
  (0, _inherits3.default)(RecoverPasswordForm, _React$PureComponent);

  function RecoverPasswordForm() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, RecoverPasswordForm);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = RecoverPasswordForm.__proto__ || (0, _getPrototypeOf2.default)(RecoverPasswordForm)).call.apply(_ref, [this].concat(args))), _this), _this.onEmailChange = function (event) {
      return _this.props.onChange({
        email: event.target.value
      });
    }, _this.onSubmit = function (event) {
      event.preventDefault();
      var email = _this.props.user.email;

      _this.props.recoverUserPassword(email);
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(RecoverPasswordForm, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          error = _props.user.error;

      var parsedAuthError = (0, _utils2.default)(error);
      return _react2.default.createElement(
        'div',
        { className: classes.root },
        _react2.default.createElement(
          _Paper2.default,
          { className: classes.paper },
          _react2.default.createElement(
            'form',
            {
              className: classes.form,
              onSubmit: this.onSubmit
            },
            _react2.default.createElement(_TextField2.default, {
              margin: 'dense',
              label: 'E-mail',
              fullWidth: true,
              required: true,
              error: parsedAuthError.field === 'EMAIL',
              helperText: parsedAuthError.message,
              InputProps: {
                inputProps: {
                  pattern: _regex.VALID_EMAIL_REGEX.source,
                  title: 'Insira um e-mail válido'
                }
              },
              onChange: this.onEmailChange
            }),
            _react2.default.createElement(
              'div',
              null,
              _react2.default.createElement(
                _Button2.default,
                {
                  className: classes.confirmButton,
                  type: 'submit',
                  color: 'accent',
                  raised: true
                },
                'Recuperar'
              )
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: classes.buttonWrapper },
          _react2.default.createElement(
            _Button2.default,
            { href: '/login' },
            'Fazer login'
          ),
          _react2.default.createElement(
            _Button2.default,
            { href: '/register' },
            'Cadastrar'
          )
        )
      );
    }
  }]);
  return RecoverPasswordForm;
}(_react2.default.PureComponent);

RecoverPasswordForm.propTypes = {
  classes: _propTypes2.default.shape({
    root: _propTypes2.default.string
  }).isRequired,
  user: _propTypes2.default.shape({
    email: _propTypes2.default.string,
    error: _propTypes2.default.object
  }).isRequired,
  onChange: _propTypes2.default.func.isRequired,
  recoverUserPassword: _propTypes2.default.func.isRequired
};

exports.default = (0, _styles.withStyles)(styles)(RecoverPasswordForm);

/***/ }),
/* 79 */,
/* 80 */,
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */,
/* 85 */,
/* 86 */,
/* 87 */,
/* 88 */,
/* 89 */,
/* 90 */,
/* 91 */,
/* 92 */,
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(94);


/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _dashboard = __webpack_require__(65);

Object.defineProperty(exports, 'Dashboard', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_dashboard).default;
  }
});

var _container = __webpack_require__(128);

Object.defineProperty(exports, 'DashboardContainer', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_container).default;
  }
});

var _theme = __webpack_require__(129);

Object.defineProperty(exports, 'Theme', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_theme).default;
  }
});

var _fixedAppbar = __webpack_require__(130);

Object.defineProperty(exports, 'FixedAppbar', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_fixedAppbar).default;
  }
});

var _toolbar = __webpack_require__(132);

Object.defineProperty(exports, 'Toolbar', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_toolbar).default;
  }
});

var _sideMenu = __webpack_require__(74);

Object.defineProperty(exports, 'SideMenu', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_sideMenu).default;
  }
});

var _container2 = __webpack_require__(135);

Object.defineProperty(exports, 'SideMenuContainer', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_container2).default;
  }
});

var _header = __webpack_require__(136);

Object.defineProperty(exports, 'SideMenuHeader', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_header).default;
  }
});

var _container3 = __webpack_require__(138);

Object.defineProperty(exports, 'SideMenuToggleContainer', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_container3).default;
  }
});

var _main = __webpack_require__(141);

Object.defineProperty(exports, 'Main', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_main).default;
  }
});

var _login = __webpack_require__(76);

Object.defineProperty(exports, 'LoginForm', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_login).default;
  }
});

var _container4 = __webpack_require__(143);

Object.defineProperty(exports, 'LoginFormContainer', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_container4).default;
  }
});

var _register = __webpack_require__(77);

Object.defineProperty(exports, 'RegisterForm', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_register).default;
  }
});

var _container5 = __webpack_require__(144);

Object.defineProperty(exports, 'RegisterFormContainer', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_container5).default;
  }
});

var _recoverPassword = __webpack_require__(78);

Object.defineProperty(exports, 'RecoverPasswordForm', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_recoverPassword).default;
  }
});

var _container6 = __webpack_require__(145);

Object.defineProperty(exports, 'RecoverPasswordFormContainer', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_container6).default;
  }
});

var _container7 = __webpack_require__(146);

Object.defineProperty(exports, 'AuthMenu', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_container7).default;
  }
});

var _search = __webpack_require__(153);

Object.defineProperty(exports, 'AppSearch', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_search).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(96);
module.exports = __webpack_require__(4).Object.getPrototypeOf;


/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.9 Object.getPrototypeOf(O)
var toObject = __webpack_require__(31);
var $getPrototypeOf = __webpack_require__(66);

__webpack_require__(97)('getPrototypeOf', function () {
  return function getPrototypeOf(it) {
    return $getPrototypeOf(toObject(it));
  };
});


/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

// most Object methods by ES6 should accept primitives
var $export = __webpack_require__(9);
var core = __webpack_require__(4);
var fails = __webpack_require__(8);
module.exports = function (KEY, exec) {
  var fn = (core.Object || {})[KEY] || Object[KEY];
  var exp = {};
  exp[KEY] = exec(fn);
  $export($export.S + $export.F * fails(function () { fn(1); }), 'Object', exp);
};


/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(99);
var $Object = __webpack_require__(4).Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(9);
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(5), 'Object', { defineProperty: __webpack_require__(10).f });


/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(101), __esModule: true };

/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(102);
__webpack_require__(107);
module.exports = __webpack_require__(58).f('iterator');


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var $at = __webpack_require__(103)(true);

// 21.1.3.27 String.prototype[@@iterator]()
__webpack_require__(69)(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});


/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__(19);
var defined = __webpack_require__(18);
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};


/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var create = __webpack_require__(56);
var descriptor = __webpack_require__(16);
var setToStringTag = __webpack_require__(57);
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__(13)(IteratorPrototype, __webpack_require__(22)('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__(10);
var anObject = __webpack_require__(14);
var getKeys = __webpack_require__(17);

module.exports = __webpack_require__(5) ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__(3).document;
module.exports = document && document.documentElement;


/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(108);
var global = __webpack_require__(3);
var hide = __webpack_require__(13);
var Iterators = __webpack_require__(55);
var TO_STRING_TAG = __webpack_require__(22)('toStringTag');

var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +
  'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +
  'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +
  'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +
  'TextTrackList,TouchList').split(',');

for (var i = 0; i < DOMIterables.length; i++) {
  var NAME = DOMIterables[i];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
  Iterators[NAME] = Iterators.Array;
}


/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var addToUnscopables = __webpack_require__(109);
var step = __webpack_require__(110);
var Iterators = __webpack_require__(55);
var toIObject = __webpack_require__(12);

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__(69)(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');


/***/ }),
/* 109 */
/***/ (function(module, exports) {

module.exports = function () { /* empty */ };


/***/ }),
/* 110 */
/***/ (function(module, exports) {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(112), __esModule: true };

/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(113);
__webpack_require__(118);
__webpack_require__(119);
__webpack_require__(120);
module.exports = __webpack_require__(4).Symbol;


/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// ECMAScript 6 symbols shim
var global = __webpack_require__(3);
var has = __webpack_require__(11);
var DESCRIPTORS = __webpack_require__(5);
var $export = __webpack_require__(9);
var redefine = __webpack_require__(70);
var META = __webpack_require__(114).KEY;
var $fails = __webpack_require__(8);
var shared = __webpack_require__(28);
var setToStringTag = __webpack_require__(57);
var uid = __webpack_require__(20);
var wks = __webpack_require__(22);
var wksExt = __webpack_require__(58);
var wksDefine = __webpack_require__(59);
var enumKeys = __webpack_require__(115);
var isArray = __webpack_require__(116);
var anObject = __webpack_require__(14);
var isObject = __webpack_require__(7);
var toIObject = __webpack_require__(12);
var toPrimitive = __webpack_require__(25);
var createDesc = __webpack_require__(16);
var _create = __webpack_require__(56);
var gOPNExt = __webpack_require__(117);
var $GOPD = __webpack_require__(72);
var $DP = __webpack_require__(10);
var $keys = __webpack_require__(17);
var gOPD = $GOPD.f;
var dP = $DP.f;
var gOPN = gOPNExt.f;
var $Symbol = global.Symbol;
var $JSON = global.JSON;
var _stringify = $JSON && $JSON.stringify;
var PROTOTYPE = 'prototype';
var HIDDEN = wks('_hidden');
var TO_PRIMITIVE = wks('toPrimitive');
var isEnum = {}.propertyIsEnumerable;
var SymbolRegistry = shared('symbol-registry');
var AllSymbols = shared('symbols');
var OPSymbols = shared('op-symbols');
var ObjectProto = Object[PROTOTYPE];
var USE_NATIVE = typeof $Symbol == 'function';
var QObject = global.QObject;
// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
var setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild;

// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
var setSymbolDesc = DESCRIPTORS && $fails(function () {
  return _create(dP({}, 'a', {
    get: function () { return dP(this, 'a', { value: 7 }).a; }
  })).a != 7;
}) ? function (it, key, D) {
  var protoDesc = gOPD(ObjectProto, key);
  if (protoDesc) delete ObjectProto[key];
  dP(it, key, D);
  if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc);
} : dP;

var wrap = function (tag) {
  var sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE]);
  sym._k = tag;
  return sym;
};

var isSymbol = USE_NATIVE && typeof $Symbol.iterator == 'symbol' ? function (it) {
  return typeof it == 'symbol';
} : function (it) {
  return it instanceof $Symbol;
};

var $defineProperty = function defineProperty(it, key, D) {
  if (it === ObjectProto) $defineProperty(OPSymbols, key, D);
  anObject(it);
  key = toPrimitive(key, true);
  anObject(D);
  if (has(AllSymbols, key)) {
    if (!D.enumerable) {
      if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}));
      it[HIDDEN][key] = true;
    } else {
      if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;
      D = _create(D, { enumerable: createDesc(0, false) });
    } return setSymbolDesc(it, key, D);
  } return dP(it, key, D);
};
var $defineProperties = function defineProperties(it, P) {
  anObject(it);
  var keys = enumKeys(P = toIObject(P));
  var i = 0;
  var l = keys.length;
  var key;
  while (l > i) $defineProperty(it, key = keys[i++], P[key]);
  return it;
};
var $create = function create(it, P) {
  return P === undefined ? _create(it) : $defineProperties(_create(it), P);
};
var $propertyIsEnumerable = function propertyIsEnumerable(key) {
  var E = isEnum.call(this, key = toPrimitive(key, true));
  if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false;
  return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true;
};
var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {
  it = toIObject(it);
  key = toPrimitive(key, true);
  if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return;
  var D = gOPD(it, key);
  if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;
  return D;
};
var $getOwnPropertyNames = function getOwnPropertyNames(it) {
  var names = gOPN(toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);
  } return result;
};
var $getOwnPropertySymbols = function getOwnPropertySymbols(it) {
  var IS_OP = it === ObjectProto;
  var names = gOPN(IS_OP ? OPSymbols : toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key]);
  } return result;
};

// 19.4.1.1 Symbol([description])
if (!USE_NATIVE) {
  $Symbol = function Symbol() {
    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
    var tag = uid(arguments.length > 0 ? arguments[0] : undefined);
    var $set = function (value) {
      if (this === ObjectProto) $set.call(OPSymbols, value);
      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
      setSymbolDesc(this, tag, createDesc(1, value));
    };
    if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set });
    return wrap(tag);
  };
  redefine($Symbol[PROTOTYPE], 'toString', function toString() {
    return this._k;
  });

  $GOPD.f = $getOwnPropertyDescriptor;
  $DP.f = $defineProperty;
  __webpack_require__(71).f = gOPNExt.f = $getOwnPropertyNames;
  __webpack_require__(21).f = $propertyIsEnumerable;
  __webpack_require__(30).f = $getOwnPropertySymbols;

  if (DESCRIPTORS && !__webpack_require__(54)) {
    redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
  }

  wksExt.f = function (name) {
    return wrap(wks(name));
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Symbol: $Symbol });

for (var es6Symbols = (
  // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
  'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'
).split(','), j = 0; es6Symbols.length > j;)wks(es6Symbols[j++]);

for (var wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) wksDefine(wellKnownSymbols[k++]);

$export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
  // 19.4.2.1 Symbol.for(key)
  'for': function (key) {
    return has(SymbolRegistry, key += '')
      ? SymbolRegistry[key]
      : SymbolRegistry[key] = $Symbol(key);
  },
  // 19.4.2.5 Symbol.keyFor(sym)
  keyFor: function keyFor(sym) {
    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');
    for (var key in SymbolRegistry) if (SymbolRegistry[key] === sym) return key;
  },
  useSetter: function () { setter = true; },
  useSimple: function () { setter = false; }
});

$export($export.S + $export.F * !USE_NATIVE, 'Object', {
  // 19.1.2.2 Object.create(O [, Properties])
  create: $create,
  // 19.1.2.4 Object.defineProperty(O, P, Attributes)
  defineProperty: $defineProperty,
  // 19.1.2.3 Object.defineProperties(O, Properties)
  defineProperties: $defineProperties,
  // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
  getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
  // 19.1.2.7 Object.getOwnPropertyNames(O)
  getOwnPropertyNames: $getOwnPropertyNames,
  // 19.1.2.8 Object.getOwnPropertySymbols(O)
  getOwnPropertySymbols: $getOwnPropertySymbols
});

// 24.3.2 JSON.stringify(value [, replacer [, space]])
$JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
  var S = $Symbol();
  // MS Edge converts symbol values to JSON as {}
  // WebKit converts symbol values to JSON as null
  // V8 throws on boxed symbols
  return _stringify([S]) != '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}';
})), 'JSON', {
  stringify: function stringify(it) {
    var args = [it];
    var i = 1;
    var replacer, $replacer;
    while (arguments.length > i) args.push(arguments[i++]);
    $replacer = replacer = args[1];
    if (!isObject(replacer) && it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
    if (!isArray(replacer)) replacer = function (key, value) {
      if (typeof $replacer == 'function') value = $replacer.call(this, key, value);
      if (!isSymbol(value)) return value;
    };
    args[1] = replacer;
    return _stringify.apply($JSON, args);
  }
});

// 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)
$Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__(13)($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf);
// 19.4.3.5 Symbol.prototype[@@toStringTag]
setToStringTag($Symbol, 'Symbol');
// 20.2.1.9 Math[@@toStringTag]
setToStringTag(Math, 'Math', true);
// 24.3.3 JSON[@@toStringTag]
setToStringTag(global.JSON, 'JSON', true);


/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

var META = __webpack_require__(20)('meta');
var isObject = __webpack_require__(7);
var has = __webpack_require__(11);
var setDesc = __webpack_require__(10).f;
var id = 0;
var isExtensible = Object.isExtensible || function () {
  return true;
};
var FREEZE = !__webpack_require__(8)(function () {
  return isExtensible(Object.preventExtensions({}));
});
var setMeta = function (it) {
  setDesc(it, META, { value: {
    i: 'O' + ++id, // object ID
    w: {}          // weak collections IDs
  } });
};
var fastKey = function (it, create) {
  // return primitive with prefix
  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return 'F';
    // not necessary to add metadata
    if (!create) return 'E';
    // add missing metadata
    setMeta(it);
  // return object ID
  } return it[META].i;
};
var getWeak = function (it, create) {
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return true;
    // not necessary to add metadata
    if (!create) return false;
    // add missing metadata
    setMeta(it);
  // return hash weak collections IDs
  } return it[META].w;
};
// add metadata on freeze-family methods calling
var onFreeze = function (it) {
  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
  return it;
};
var meta = module.exports = {
  KEY: META,
  NEED: false,
  fastKey: fastKey,
  getWeak: getWeak,
  onFreeze: onFreeze
};


/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

// all enumerable object keys, includes symbols
var getKeys = __webpack_require__(17);
var gOPS = __webpack_require__(30);
var pIE = __webpack_require__(21);
module.exports = function (it) {
  var result = getKeys(it);
  var getSymbols = gOPS.f;
  if (getSymbols) {
    var symbols = getSymbols(it);
    var isEnum = pIE.f;
    var i = 0;
    var key;
    while (symbols.length > i) if (isEnum.call(it, key = symbols[i++])) result.push(key);
  } return result;
};


/***/ }),
/* 116 */
/***/ (function(module, exports, __webpack_require__) {

// 7.2.2 IsArray(argument)
var cof = __webpack_require__(45);
module.exports = Array.isArray || function isArray(arg) {
  return cof(arg) == 'Array';
};


/***/ }),
/* 117 */
/***/ (function(module, exports, __webpack_require__) {

// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
var toIObject = __webpack_require__(12);
var gOPN = __webpack_require__(71).f;
var toString = {}.toString;

var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
  ? Object.getOwnPropertyNames(window) : [];

var getWindowNames = function (it) {
  try {
    return gOPN(it);
  } catch (e) {
    return windowNames.slice();
  }
};

module.exports.f = function getOwnPropertyNames(it) {
  return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it));
};


/***/ }),
/* 118 */
/***/ (function(module, exports) {



/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(59)('asyncIterator');


/***/ }),
/* 120 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(59)('observable');


/***/ }),
/* 121 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(122), __esModule: true };

/***/ }),
/* 122 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(123);
module.exports = __webpack_require__(4).Object.setPrototypeOf;


/***/ }),
/* 123 */
/***/ (function(module, exports, __webpack_require__) {

// 19.1.3.19 Object.setPrototypeOf(O, proto)
var $export = __webpack_require__(9);
$export($export.S, 'Object', { setPrototypeOf: __webpack_require__(124).set });


/***/ }),
/* 124 */
/***/ (function(module, exports, __webpack_require__) {

// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var isObject = __webpack_require__(7);
var anObject = __webpack_require__(14);
var check = function (O, proto) {
  anObject(O);
  if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!");
};
module.exports = {
  set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
    function (test, buggy, set) {
      try {
        set = __webpack_require__(41)(Function.call, __webpack_require__(72).f(Object.prototype, '__proto__').set, 2);
        set(test, []);
        buggy = !(test instanceof Array);
      } catch (e) { buggy = true; }
      return function setPrototypeOf(O, proto) {
        check(O, proto);
        if (buggy) O.__proto__ = proto;
        else set(O, proto);
        return O;
      };
    }({}, false) : undefined),
  check: check
};


/***/ }),
/* 125 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(126), __esModule: true };

/***/ }),
/* 126 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(127);
var $Object = __webpack_require__(4).Object;
module.exports = function create(P, D) {
  return $Object.create(P, D);
};


/***/ }),
/* 127 */
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__(9);
// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
$export($export.S, 'Object', { create: __webpack_require__(56) });


/***/ }),
/* 128 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactRedux = __webpack_require__(15);

var _index = __webpack_require__(65);

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mapStateToProps = function mapStateToProps(state) {
  return {
    open: state.get('dashboard').sideMenu.open
  };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps)(_index2.default);

/***/ }),
/* 129 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(40);

var _extends3 = _interopRequireDefault(_extends2);

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(2);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = __webpack_require__(6);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Theme = function Theme(_ref) {
  var children = _ref.children,
      customTheme = _ref.customTheme;

  var theme = (0, _styles.createMuiTheme)((0, _extends3.default)({}, customTheme));

  return _react2.default.createElement(
    _styles.MuiThemeProvider,
    { theme: theme },
    children
  );
};

Theme.defaultProps = {
  customTheme: null
};

Theme.propTypes = {
  children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]).isRequired,
  customTheme: _propTypes2.default.shape({ '*': _propTypes2.default.object })
};

exports.default = Theme;

/***/ }),
/* 130 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(2);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _AppBar = __webpack_require__(131);

var _AppBar2 = _interopRequireDefault(_AppBar);

var _styles = __webpack_require__(6);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles() {
  return {
    root: {
      width: '100%',
      height: '100%',
      position: 'relative',
      display: 'flex'
    },
    appBar: {
      position: 'fixed',
      top: '0',
      left: 'auto',
      right: 'auto'
    }
  };
};

var FixedAppbar = function FixedAppbar(_ref) {
  var children = _ref.children,
      classes = _ref.classes;
  return _react2.default.createElement(
    _AppBar2.default,
    {
      className: classes.appBar,
      position: 'fixed'
    },
    children
  );
};

FixedAppbar.propTypes = {
  children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]).isRequired,
  classes: _propTypes2.default.shape({
    root: _propTypes2.default.string,
    appBar: _propTypes2.default.string
  }).isRequired
};

exports.default = (0, _styles.withStyles)(styles)(FixedAppbar);

/***/ }),
/* 131 */
/***/ (function(module, exports) {

module.exports = require("material-ui/AppBar");

/***/ }),
/* 132 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(2);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Toolbar = __webpack_require__(133);

var _Toolbar2 = _interopRequireDefault(_Toolbar);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Toolbar = function Toolbar(_ref) {
  var children = _ref.children;
  return _react2.default.createElement(
    _Toolbar2.default,
    { disableGutters: true },
    children
  );
};

Toolbar.propTypes = {
  children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]).isRequired
};

exports.default = Toolbar;

/***/ }),
/* 133 */
/***/ (function(module, exports) {

module.exports = require("material-ui/Toolbar");

/***/ }),
/* 134 */
/***/ (function(module, exports) {

module.exports = require("material-ui/Drawer");

/***/ }),
/* 135 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactRedux = __webpack_require__(15);

var _index = __webpack_require__(74);

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mapStateToProps = function mapStateToProps(state) {
  return {
    open: state.get('dashboard').sideMenu.open
  };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps)(_index2.default);

/***/ }),
/* 136 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(40);

var _extends3 = _interopRequireDefault(_extends2);

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(2);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Divider = __webpack_require__(137);

var _Divider2 = _interopRequireDefault(_Divider);

var _styles = __webpack_require__(6);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    drawerHeader: (0, _extends3.default)({
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-start',
      padding: '0 8px'
    }, theme.mixins.toolbar)
  };
};

var SideMenuHeader = function SideMenuHeader(_ref) {
  var classes = _ref.classes,
      children = _ref.children;
  return _react2.default.createElement(
    'div',
    null,
    _react2.default.createElement(
      'div',
      { className: classes.drawerHeader },
      children
    ),
    _react2.default.createElement(_Divider2.default, null)
  );
};

SideMenuHeader.propTypes = {
  classes: _propTypes2.default.shape({
    drawerHeader: _propTypes2.default.string
  }).isRequired
};

exports.default = (0, _styles.withStyles)(styles)(SideMenuHeader);

/***/ }),
/* 137 */
/***/ (function(module, exports) {

module.exports = require("material-ui/Divider");

/***/ }),
/* 138 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactRedux = __webpack_require__(15);

var _toggleButton = __webpack_require__(139);

var _toggleButton2 = _interopRequireDefault(_toggleButton);

var _actions = __webpack_require__(0);

var _actions2 = _interopRequireDefault(_actions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    toggleSideMenu: function toggleSideMenu() {
      return dispatch({
        type: _actions2.default.TOGGLE_SIDE_MENU
      });
    }
  };
};

exports.default = (0, _reactRedux.connect)(null, mapDispatchToProps)(_toggleButton2.default);

/***/ }),
/* 139 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(2);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _IconButton = __webpack_require__(75);

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Menu = __webpack_require__(140);

var _Menu2 = _interopRequireDefault(_Menu);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SideMenuToggle = function SideMenuToggle(_ref) {
  var toggleSideMenu = _ref.toggleSideMenu;
  return _react2.default.createElement(
    _IconButton2.default,
    {
      color: 'contrast',
      'aria-label': 'Menu',
      onClick: toggleSideMenu
    },
    _react2.default.createElement(_Menu2.default, null)
  );
};

SideMenuToggle.propTypes = {
  toggleSideMenu: _propTypes2.default.func.isRequired
};

exports.default = SideMenuToggle;

/***/ }),
/* 140 */
/***/ (function(module, exports) {

module.exports = require("material-ui-icons/Menu");

/***/ }),
/* 141 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = __webpack_require__(142);

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(2);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = __webpack_require__(6);

var _classnames = __webpack_require__(73);

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    content: (0, _defineProperty3.default)({
      width: '100%',
      overflowX: 'hidden',
      height: 'calc(100% - ' + theme.spacing.unit * 7 + 'px)',
      transform: 'translate(0px, ' + theme.spacing.unit * 7 + 'px)'
    }, theme.breakpoints.up('sm'), {
      height: 'calc(100% - ' + theme.spacing.unit * 8 + 'px)',
      transform: 'translate(0px, ' + theme.spacing.unit * 8 + 'px)'
    })
  };
};

var Main = function Main(_ref) {
  var children = _ref.children,
      classes = _ref.classes,
      className = _ref.className,
      style = _ref.style;
  return _react2.default.createElement(
    'main',
    { className: (0, _classnames2.default)(className, classes.content), style: style },
    children
  );
};

Main.defaultProps = {
  style: null,
  className: null
};

Main.propTypes = {
  style: _propTypes2.default.shape({}),
  children: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.node), _propTypes2.default.node]).isRequired,
  classes: _propTypes2.default.shape({
    root: _propTypes2.default.string,
    appBar: _propTypes2.default.string
  }).isRequired,
  className: _propTypes2.default.string
};

exports.default = (0, _styles.withStyles)(styles)(Main);

/***/ }),
/* 142 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(67);

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (obj, key, value) {
  if (key in obj) {
    (0, _defineProperty2.default)(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
};

/***/ }),
/* 143 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactRedux = __webpack_require__(15);

var _actions = __webpack_require__(0);

var _actions2 = _interopRequireDefault(_actions);

var _index = __webpack_require__(76);

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mapStateToProps = function mapStateToProps(state) {
  return {
    user: state.get('user')
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onChange: function onChange(value) {
      return dispatch({
        type: _actions2.default.UPDATED_FORM,
        payload: value
      });
    },
    loginUser: function loginUser(email, password) {
      return dispatch({
        type: _actions2.default.LOGIN_USER,
        payload: { email: email, password: password }
      });
    },
    loginWithFacebook: function loginWithFacebook() {
      return dispatch({
        type: _actions2.default.LOGIN_USER_WITH_FACEBOOK
      });
    },
    loginWithGoogle: function loginWithGoogle() {
      return dispatch({
        type: _actions2.default.LOGIN_USER_WITH_GOOGLE
      });
    }
  };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(_index2.default);

/***/ }),
/* 144 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactRedux = __webpack_require__(15);

var _index = __webpack_require__(77);

var _index2 = _interopRequireDefault(_index);

var _actions = __webpack_require__(0);

var _actions2 = _interopRequireDefault(_actions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mapStateToProps = function mapStateToProps(state) {
  return {
    user: state.get('user')
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onChange: function onChange(value) {
      return dispatch({
        type: _actions2.default.UPDATED_FORM,
        payload: value
      });
    },
    registerNewUser: function registerNewUser(email, password) {
      return dispatch({
        type: _actions2.default.REGISTER_NEW_USER,
        payload: { email: email, password: password }
      });
    }
  };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(_index2.default);

/***/ }),
/* 145 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactRedux = __webpack_require__(15);

var _index = __webpack_require__(78);

var _index2 = _interopRequireDefault(_index);

var _actions = __webpack_require__(0);

var _actions2 = _interopRequireDefault(_actions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mapStateToProps = function mapStateToProps(state) {
  return {
    user: state.get('user')
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onChange: function onChange(value) {
      return dispatch({
        type: _actions2.default.UPDATED_FORM,
        payload: value
      });
    },
    recoverUserPassword: function recoverUserPassword(email) {
      return dispatch({
        type: _actions2.default.RECOVER_USER_PASSWORD,
        payload: { email: email }
      });
    }
  };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(_index2.default);

/***/ }),
/* 146 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reactRedux = __webpack_require__(15);

var _actions = __webpack_require__(0);

var _actions2 = _interopRequireDefault(_actions);

var _index = __webpack_require__(147);

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    logoutUser: function logoutUser() {
      return dispatch({
        type: _actions2.default.LOGOUT_USER
      });
    }
  };
};

exports.default = (0, _reactRedux.connect)(null, mapDispatchToProps)(_index2.default);

/***/ }),
/* 147 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(35);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(36);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(37);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(38);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(39);

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(2);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = __webpack_require__(6);

var _Avatar = __webpack_require__(148);

var _Avatar2 = _interopRequireDefault(_Avatar);

var _Menu = __webpack_require__(149);

var _Menu2 = _interopRequireDefault(_Menu);

var _IconButton = __webpack_require__(75);

var _IconButton2 = _interopRequireDefault(_IconButton);

var _AccountCircle = __webpack_require__(150);

var _AccountCircle2 = _interopRequireDefault(_AccountCircle);

var _Typography = __webpack_require__(151);

var _Typography2 = _interopRequireDefault(_Typography);

var _blueGrey = __webpack_require__(152);

var _blueGrey2 = _interopRequireDefault(_blueGrey);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    display: 'flex'
  },
  userInfo: {
    display: 'flex',
    flexDirection: 'column',
    padding: 5
  },
  iconButton: {
    color: _blueGrey2.default[100]
  },
  iconCircle: {
    width: 40,
    height: 40
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  bigAvatar: {
    backgroundColor: _blueGrey2.default[50],
    width: 60,
    height: 60
  }
};

var AuthMenu = function (_React$Component) {
  (0, _inherits3.default)(AuthMenu, _React$Component);

  function AuthMenu() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, AuthMenu);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = AuthMenu.__proto__ || (0, _getPrototypeOf2.default)(AuthMenu)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      auth: true,
      anchorEl: null
    }, _this.handleChange = function (event, checked) {
      _this.setState({ auth: checked });
    }, _this.handleMenu = function (event) {
      _this.setState({ anchorEl: event.currentTarget });
    }, _this.handleClose = function () {
      _this.setState({ anchorEl: null });
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(AuthMenu, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          classes = _props.classes,
          _props$userInfo = _props.userInfo,
          name = _props$userInfo.name,
          email = _props$userInfo.email,
          logoutUser = _props.logoutUser;
      var anchorEl = this.state.anchorEl;

      var open = Boolean(anchorEl);

      return _react2.default.createElement(
        'div',
        { className: classes.root },
        _react2.default.createElement(
          _Avatar2.default,
          { className: classes.bigAvatar },
          _react2.default.createElement(
            _IconButton2.default,
            {
              className: classes.iconButton,
              'aria-owns': open ? 'menu-auth' : null,
              'aria-haspopup': 'true',
              onClick: this.handleMenu,
              color: 'contrast'
            },
            _react2.default.createElement(_AccountCircle2.default, { className: classes.iconCircle })
          )
        ),
        _react2.default.createElement(
          _Menu2.default,
          {
            id: 'menu-auth',
            anchorEl: anchorEl,
            anchorOrigin: {
              vertical: 'top',
              horizontal: 'right'
            },
            transformOrigin: {
              vertical: 'top',
              horizontal: 'right'
            },
            open: open,
            onClose: this.handleClose
          },
          _react2.default.createElement(
            _Menu.MenuItem,
            { onClick: this.handleClose },
            'Profile'
          ),
          _react2.default.createElement(
            _Menu.MenuItem,
            { onClick: function onClick() {
                _this2.handleClose();logoutUser();
              } },
            'Logout'
          )
        ),
        _react2.default.createElement(
          'div',
          { className: classes.userInfo },
          _react2.default.createElement(
            _Typography2.default,
            {
              gutterBottom: true,
              align: 'left',
              type: 'title',
              color: 'inherit'
            },
            name
          ),
          _react2.default.createElement(
            _Typography2.default,
            {
              gutterBottom: true,
              align: 'left',
              type: 'caption',
              color: 'inherit'
            },
            email
          )
        )
      );
    }
  }]);
  return AuthMenu;
}(_react2.default.Component);

AuthMenu.propTypes = {
  classes: _propTypes2.default.shape({
    root: _propTypes2.default.string
  }).isRequired,
  userInfo: _propTypes2.default.shape({
    name: _propTypes2.default.string,
    email: _propTypes2.default.string
  }).isRequired,
  logoutUser: _propTypes2.default.func.isRequired
};

exports.default = (0, _styles.withStyles)(styles)(AuthMenu);

/***/ }),
/* 148 */
/***/ (function(module, exports) {

module.exports = require("material-ui/Avatar");

/***/ }),
/* 149 */
/***/ (function(module, exports) {

module.exports = require("material-ui/Menu");

/***/ }),
/* 150 */
/***/ (function(module, exports) {

module.exports = require("material-ui-icons/AccountCircle");

/***/ }),
/* 151 */
/***/ (function(module, exports) {

module.exports = require("material-ui/Typography");

/***/ }),
/* 152 */
/***/ (function(module, exports) {

module.exports = require("material-ui/colors/blueGrey");

/***/ }),
/* 153 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _compose = __webpack_require__(154);

var _compose2 = _interopRequireDefault(_compose);

var _pure = __webpack_require__(155);

var _pure2 = _interopRequireDefault(_pure);

var _propTypes = __webpack_require__(2);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withWidth = __webpack_require__(156);

var _withWidth2 = _interopRequireDefault(_withWidth);

var _Search = __webpack_require__(157);

var _Search2 = _interopRequireDefault(_Search);

var _colorManipulator = __webpack_require__(158);

var _styles = __webpack_require__(6);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    wrapper: {
      fontFamily: theme.typography.fontFamily,
      position: 'relative',
      marginRight: 16,
      borderRadius: 2,
      background: (0, _colorManipulator.fade)(theme.palette.common.white, 0.15),
      '&:hover': {
        background: (0, _colorManipulator.fade)(theme.palette.common.white, 0.25)
      },
      '& $input': {
        transition: theme.transitions.create('width'),
        width: 200,
        '&:focus': {
          width: 220
        }
      }
    },
    search: {
      width: theme.spacing.unit * 9,
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    input: {
      font: 'inherit',
      padding: theme.spacing.unit + 'px ' + theme.spacing.unit + 'px ' + theme.spacing.unit + 'px ' + theme.spacing.unit * 9 + 'px',
      border: 0,
      display: 'block',
      verticalAlign: 'middle',
      whiteSpace: 'normal',
      background: 'none',
      margin: 0, // Reset for Safari
      color: 'inherit',
      width: '100%',
      '&:focus': {
        outline: 0
      }
    }
  };
};

function AppSearch(props) {
  var classes = props.classes;


  return _react2.default.createElement(
    'div',
    { className: classes.wrapper },
    _react2.default.createElement(
      'div',
      { className: classes.search },
      _react2.default.createElement(_Search2.default, null)
    ),
    _react2.default.createElement('input', { id: 'search-input', className: classes.input })
  );
}

AppSearch.propTypes = {
  classes: _propTypes2.default.object.isRequired
};

exports.default = (0, _compose2.default)((0, _styles.withStyles)(styles, {
  name: 'AppSearch'
}), (0, _withWidth2.default)(), _pure2.default)(AppSearch);

/***/ }),
/* 154 */
/***/ (function(module, exports) {

module.exports = require("recompose/compose");

/***/ }),
/* 155 */
/***/ (function(module, exports) {

module.exports = require("recompose/pure");

/***/ }),
/* 156 */
/***/ (function(module, exports) {

module.exports = require("material-ui/utils/withWidth");

/***/ }),
/* 157 */
/***/ (function(module, exports) {

module.exports = require("material-ui-icons/Search");

/***/ }),
/* 158 */
/***/ (function(module, exports) {

module.exports = require("material-ui/styles/colorManipulator");

/***/ })
/******/ ]);
});