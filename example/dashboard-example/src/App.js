import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { ConnectedRouter, routerMiddleware, push } from 'react-router-redux';
import { combineReducers } from 'redux-immutable';
import createHistory from 'history/createBrowserHistory';
import { Switch } from 'react-router';
import { Route, Link } from 'react-router-dom';

import List, { ListItem, ListItemText } from 'material-ui/List';
import { createMuiTheme } from 'material-ui/styles';
import blue from 'material-ui/colors/blue';
import blueGrey from 'material-ui/colors/blueGrey';

import { reducers, sagas, firebase } from 'react-dashboard-mui/Api';

import {
  Theme,
  AppSearch,
  DashboardContainer as Dashboard,
  FixedAppbar,
  Toolbar,
  SideMenuContainer as SideMenu,
  SideMenuHeader,
  SideMenuToggleContainer as SideMenuToggle,
  Main,
  LoginFormContainer as LoginForm,
  RegisterFormContainer as RegisterForm,
  RecoverPasswordFormContainer as RecoverPasswordForm,
  AuthMenu,
} from 'react-dashboard-mui/Components';

const theme = createMuiTheme({
  palette: {
    primary: { light: blue[100], main: blue[100], dark: blue[700] },
    secondary: { light: blueGrey[300], main: blueGrey[500], dark: blueGrey[700] },
  },
});

class App extends Component {
  componentWillMount() {
    this.history = createHistory();
    const sagaMiddleware = createSagaMiddleware();
    const middlewares = [
      routerMiddleware(this.history),
      sagaMiddleware,
    ];
    const combinedReducers = combineReducers(reducers);
    const composeEnhancers = composeWithDevTools({});
    const composed = composeEnhancers(applyMiddleware(...middlewares));
    this.store = createStore(combinedReducers, composed);
    sagaMiddleware.run(sagas, firebase);
  }

  createMenuItem = (pageHref, pageName) => (
    <ListItem key={pageHref} button component={Link} to={pageHref}>
      <ListItemText primary={pageName} />
    </ListItem>
  );

  createPageRoute = (pageHref, component) => (
    <Route key={pageHref} path={pageHref} component={component} />
  );

  render() {
    const links = [
      this.createMenuItem('/one', 'One'),
      this.createMenuItem('/two', 'Two'),
      this.createMenuItem('/three', 'Three'),
    ];

    const pages = [
      this.createPageRoute('/one', () => <div>One</div>),
      this.createPageRoute('/two', () => <div>Two</div>),
      this.createPageRoute('/three', () => <div>Three</div>),
    ];

    return (
      <Provider store={this.store}>
        <ConnectedRouter history={this.history}>
          <Theme customTheme={theme}>
            <Switch>
              <Route path="/login" component={LoginForm} />
              <Route path="/register" component={RegisterForm} />
              <Route path="/recover-password" component={RecoverPasswordForm} />
              <Route
                path="/"
                render={() => {
                  firebase.auth().onAuthStateChanged((user) => {
                    if (!user) {
                      this.store.dispatch(push('/login'));
                    }
                  });
                  return (
                    <Dashboard>
                      <FixedAppbar>
                        <Toolbar>
                          <SideMenuToggle />
                          <AppSearch />
                        </Toolbar>
                      </FixedAppbar>
                      <SideMenu>
                        <SideMenuHeader>
                          <AuthMenu userInfo={{
                            name: 'John Doe',
                            email: 'johndoe@gmail.com',
                          }}
                          />
                        </SideMenuHeader>
                        <List>
                          {links}
                        </List>
                      </SideMenu>
                      <Main style={{ padding: 16 }}>
                        {pages}
                      </Main>
                    </Dashboard>
                  );
                }}
              />
            </Switch>
          </Theme>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
