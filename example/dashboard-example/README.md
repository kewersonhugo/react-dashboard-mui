# Configurando o Firebase do App

Para configurar o Firebase, crie um arquivo chamado `.env.local` e `.env.test.local`, e preencha as seguintes variaveis com suas respectivas informações do console do Firebase:

```
REACT_APP_FIREBASE_API_KEY
REACT_APP_FIREBASE_AUTH_DOMAIN
REACT_APP_FIREBASE_DATABASE_URL
REACT_APP_FIREBASE_PROJECT_ID
REACT_APP_FIREBASE_STORAGE_BUCKET
REACT_APP_FIREBASE_SENDER_ID
```