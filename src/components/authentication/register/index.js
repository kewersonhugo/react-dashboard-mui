import React from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import { VALID_EMAIL_REGEX } from 'api/regex';
import parseFirebaseAuthError from 'api/firebase/utils';

const styles = theme => ({
  root: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  paper: {
    padding: '32px',
  },
  form: {
    width: 'auto',
    maxWidth: '400px',
  },
  confirmButton: {
    width: '100%',
    marginTop: theme.spacing.unit,
  },
  buttonWrapper: {
    marginTop: theme.spacing.unit * 2,
  },
});

class RegisterForm extends React.PureComponent {
  onEmailChange = event => this.props.onChange({
    email: event.target.value,
  });

  onPasswordChange = event => this.props.onChange({
    password: event.target.value,
  });

  onConfirmPasswordChange = event => this.props.onChange({
    confirmPassword: event.target.value,
  });

  onSubmit = (event) => {
    event.preventDefault();
    const { email, password, confirmPassword } = this.props.user;
    if (confirmPassword === password) this.props.registerNewUser(email, password);
  }

  render() {
    const { classes } = this.props;
    const { password, confirmPassword, error } = this.props.user;
    const parsedAuthError = parseFirebaseAuthError(error);
    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <form
            className={classes.form}
            onSubmit={this.onSubmit}
          >
            <TextField
              margin="dense"
              type="text"
              label="E-mail"
              fullWidth
              required
              error={parsedAuthError.field === 'EMAIL'}
              helperText={parsedAuthError.message}
              InputProps={{
                inputProps: {
                  pattern: VALID_EMAIL_REGEX.source,
                  title: 'Insira um e-mail válido',
                },
              }}
              onChange={this.onEmailChange}
            />
            <TextField
              margin="dense"
              label="Senha"
              fullWidth
              required
              error={parsedAuthError.field === 'PASSWORD'}
              helperText={parsedAuthError.message}
              InputProps={{
                inputProps: {
                  type: 'password',
                  minLength: '6',
                },
              }}
              onChange={this.onPasswordChange}
            />
            <TextField
              margin="dense"
              type="password"
              label="Confirmar senha"
              fullWidth
              required
              error={confirmPassword !== password}
              onChange={this.onConfirmPasswordChange}
            />
            <div>
              <Button
                className={classes.confirmButton}
                type="submit"
                color="accent"
                raised
              >
                Cadastrar
              </Button>
            </div>
          </form>
        </Paper>
        <div className={classes.buttonWrapper}>
          <Button href="/login">
            Já tenho uma conta
          </Button>
        </div>
      </div>
    );
  }
}

RegisterForm.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
  }).isRequired,
  user: PropTypes.shape({
    email: PropTypes.string,
    password: PropTypes.string,
    confirmPassword: PropTypes.string,
    error: PropTypes.object,
  }).isRequired,
  onChange: PropTypes.func.isRequired,
  registerNewUser: PropTypes.func.isRequired,
};

export default withStyles(styles)(RegisterForm);
