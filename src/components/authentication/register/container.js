import { connect } from 'react-redux';
import RegisterForm from './index';
import actions from 'api/actions';

const mapStateToProps = state => ({
  user: state.get('user'),
});

const mapDispatchToProps = dispatch => ({
  onChange: value => dispatch({
    type: actions.UPDATED_FORM,
    payload: value,
  }),
  registerNewUser: (email, password) => dispatch({
    type: actions.REGISTER_NEW_USER,
    payload: { email, password },
  }),
});

export default connect(mapStateToProps, mapDispatchToProps)(RegisterForm);

