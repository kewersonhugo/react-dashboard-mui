import { connect } from 'react-redux';
import RecoverPasswordForm from './index';
import actions from 'api/actions';

const mapStateToProps = state => ({
  user: state.get('user'),
});

const mapDispatchToProps = dispatch => ({
  onChange: value => dispatch({
    type: actions.UPDATED_FORM,
    payload: value,
  }),
  recoverUserPassword: email => dispatch({
    type: actions.RECOVER_USER_PASSWORD,
    payload: { email },
  }),
});

export default connect(mapStateToProps, mapDispatchToProps)(RecoverPasswordForm);
