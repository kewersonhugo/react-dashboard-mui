import React from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import { VALID_EMAIL_REGEX } from 'api/regex';
import parseFirebaseAuthError from 'api/firebase/utils';

const styles = theme => ({
  root: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  paper: {
    padding: '32px',
  },
  form: {
    width: 'auto',
    maxWidth: '400px',
  },
  confirmButton: {
    width: '100%',
    marginTop: theme.spacing.unit,
  },
  buttonWrapper: {
    marginTop: theme.spacing.unit * 2,
  },
});

class RecoverPasswordForm extends React.PureComponent {
  onEmailChange = event => this.props.onChange({
    email: event.target.value,
  });

  onSubmit = (event) => {
    event.preventDefault();
    const { email } = this.props.user;
    this.props.recoverUserPassword(email);
  }

  render() {
    const { classes, user: { error } } = this.props;
    const parsedAuthError = parseFirebaseAuthError(error);
    return (
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <form
            className={classes.form}
            onSubmit={this.onSubmit}
          >
            <TextField
              margin="dense"
              label="E-mail"
              fullWidth
              required
              error={parsedAuthError.field === 'EMAIL'}
              helperText={parsedAuthError.message}
              InputProps={{
                inputProps: {
                  pattern: VALID_EMAIL_REGEX.source,
                  title: 'Insira um e-mail válido',
                },
              }}
              onChange={this.onEmailChange}
            />
            <div>
              <Button
                className={classes.confirmButton}
                type="submit"
                color="accent"
                raised
              >
                Recuperar
              </Button>
            </div>
          </form>
        </Paper>
        <div className={classes.buttonWrapper}>
          <Button href="/login">
            Fazer login
          </Button>
          <Button href="/register">
            Cadastrar
          </Button>
        </div>
      </div>
    );
  }
}

RecoverPasswordForm.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
  }).isRequired,
  user: PropTypes.shape({
    email: PropTypes.string,
    error: PropTypes.object,
  }).isRequired,
  onChange: PropTypes.func.isRequired,
  recoverUserPassword: PropTypes.func.isRequired,
};

export default withStyles(styles)(RecoverPasswordForm);
