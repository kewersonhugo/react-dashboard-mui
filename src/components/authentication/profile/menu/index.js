import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Avatar from 'material-ui/Avatar';
import Menu, { MenuItem } from 'material-ui/Menu';
import IconButton from 'material-ui/IconButton';
import AccountCircle from 'material-ui-icons/AccountCircle';
import Typography from 'material-ui/Typography';
import blueGrey from 'material-ui/colors/blueGrey';

const styles = {
  root: {
    display: 'flex',
  },
  userInfo: {
    display: 'flex',
    flexDirection: 'column',
    padding: 5,
  },
  iconButton: {
    color: blueGrey[100],
  },
  iconCircle: {
    width: 40,
    height: 40,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  bigAvatar: {
    backgroundColor: blueGrey[50],
    width: 60,
    height: 60,
  },
};

class AuthMenu extends React.Component {
  state = {
    auth: true,
    anchorEl: null,
  };

  handleChange = (event, checked) => {
    this.setState({ auth: checked });
  };

  handleMenu = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { classes, userInfo: { name, email }, logoutUser } = this.props;
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);

    return (
      <div className={classes.root}>
        <Avatar className={classes.bigAvatar}>
          <IconButton
            className={classes.iconButton}
            aria-owns={open ? 'menu-auth' : null}
            aria-haspopup="true"
            onClick={this.handleMenu}
            color="contrast"
          >
            <AccountCircle className={classes.iconCircle} />
          </IconButton>
        </Avatar>
        <Menu
          id="menu-auth"
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={open}
          onClose={this.handleClose}
        >
          <MenuItem onClick={this.handleClose}>Profile</MenuItem>
          <MenuItem onClick={() => { this.handleClose(); logoutUser(); }}>Logout</MenuItem>
        </Menu>
        <div className={classes.userInfo}>
          <Typography
            gutterBottom
            align="left"
            type="title"
            color="inherit"
          >
            {name}
          </Typography>
          <Typography
            gutterBottom
            align="left"
            type="caption"
            color="inherit"
          >
            {email}
          </Typography>
        </div>
      </div>
    );
  }
}

AuthMenu.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
  }).isRequired,
  userInfo: PropTypes.shape({
    name: PropTypes.string,
    email: PropTypes.string,
  }).isRequired,
  logoutUser: PropTypes.func.isRequired,
};

export default withStyles(styles)(AuthMenu);
