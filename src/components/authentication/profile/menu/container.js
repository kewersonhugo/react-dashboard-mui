import { connect } from 'react-redux';
import actions from 'api/actions';
import AuthMenu from './index';

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch({
    type: actions.LOGOUT_USER,
  }),
});

export default connect(null, mapDispatchToProps)(AuthMenu);
