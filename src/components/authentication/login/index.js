import React from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import { withStyles } from 'material-ui/styles';
import { VALID_EMAIL_REGEX } from 'api/regex';
import parseFirebaseAuthError from 'api/firebase/utils';

const styles = theme => ({
  root: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonWrapper: {
    display: 'flex',
    textAlign: 'center',
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2,
    '& > button, > a': {
      margin: '8px',
    },
  },
  facebook: {
    backgroundColor: '#3b5998',
    color: 'white',
  },
  google: {
    backgroundColor: '#d34836',
    color: 'white',
  },
  paper: {
    padding: '32px',
  },
  form: {
    width: 'auto',
    maxWidth: '400px',
  },
  confirmButton: {
    width: '100%',
    marginTop: theme.spacing.unit,
  },
});

class Login extends React.PureComponent {
  onEmailChange = event => this.props.onChange({
    email: event.target.value,
  });

  onPasswordChange = event => this.props.onChange({
    password: event.target.value,
  });

  onSubmit = (event) => {
    event.preventDefault();
    const { email, password } = this.props.user;
    this.props.loginUser(email, password);
  }

  render() {
    const { classes, user: { error } } = this.props;
    const parsedAuthError = parseFirebaseAuthError(error);
    return (
      <div className={classes.root}>
        <div className={classes.buttonWrapper}>
          <Button
            className={classes.facebook}
            onClick={this.props.loginWithFacebook}
          >
            Login com Facebook
          </Button>
          <Button
            className={classes.google}
            onClick={this.props.loginWithGoogle}
          >
            Login com Google
          </Button>
        </div>
        <Paper className={classes.paper}>
          <form
            className={classes.form}
            onSubmit={this.onSubmit}
          >
            <TextField
              margin="dense"
              label="E-mail"
              fullWidth
              required
              error={parsedAuthError.field === 'EMAIL'}
              helperText={parsedAuthError.message}
              InputProps={{
                inputProps: {
                  pattern: VALID_EMAIL_REGEX.source,
                  title: 'Insira um e-mail válido',
                },
              }}
              onChange={this.onEmailChange}
            />
            <TextField
              margin="dense"
              type="password"
              label="Password"
              fullWidth
              required
              error={parsedAuthError.field === 'PASSWORD'}
              helperText={parsedAuthError.message}
              onChange={this.onPasswordChange}
            />
            <div>
              <Button
                className={classes.confirmButton}
                type="submit"
                color="accent"
                raised
              >
                Login
              </Button>
            </div>
          </form>
        </Paper>
        <div className={classes.buttonWrapper}>
          <Button href="/recover-password">
            Esqueci minha senha
          </Button>
          <Button href="/register">
            Cadastrar
          </Button>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
  }).isRequired,
  user: PropTypes.shape({
    email: PropTypes.string,
    password: PropTypes.string,
    error: PropTypes.object,
  }).isRequired,
  onChange: PropTypes.func.isRequired,
  loginUser: PropTypes.func.isRequired,
  loginWithFacebook: PropTypes.func.isRequired,
  loginWithGoogle: PropTypes.func.isRequired,
};

export default withStyles(styles)(Login);
