import { connect } from 'react-redux';
import actions from 'api/actions';
import LoginForm from './index';

const mapStateToProps = state => ({
  user: state.get('user'),
});

const mapDispatchToProps = dispatch => ({
  onChange: value => dispatch({
    type: actions.UPDATED_FORM,
    payload: value,
  }),
  loginUser: (email, password) => dispatch({
    type: actions.LOGIN_USER,
    payload: { email, password },
  }),
  loginWithFacebook: () => dispatch({
    type: actions.LOGIN_USER_WITH_FACEBOOK,
  }),
  loginWithGoogle: () => dispatch({
    type: actions.LOGIN_USER_WITH_GOOGLE,
  }),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);

