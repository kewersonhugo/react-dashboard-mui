import { connect } from 'react-redux';
import SideMenu from './index';

const mapStateToProps = state => ({
  open: state.get('dashboard').sideMenu.open,
});

export default connect(mapStateToProps)(SideMenu);
