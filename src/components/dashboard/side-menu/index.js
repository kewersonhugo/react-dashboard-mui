import React from 'react';
import PropTypes from 'prop-types';
import Drawer from 'material-ui/Drawer';
import { withStyles } from 'material-ui/styles';

const drawerWidth = 240;

const styles = () => ({
  drawerPaper: {
    position: 'fixed',
    height: '100%',
    width: drawerWidth,
  },
});

const SideMenu = ({ children, classes, open }) => (
  <Drawer
    classes={{
      paper: classes.drawerPaper,
    }}
    type="persistent"
    open={open}
  >
    {children}
  </Drawer>
);

SideMenu.defaultProps = {
  children: null,
  open: false,
};

SideMenu.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  classes: PropTypes.shape({
    drawerPaper: PropTypes.string,
  }).isRequired,
  open: PropTypes.bool,
};

export default withStyles(styles)(SideMenu);
