import React from 'react';
import PropTypes from 'prop-types';
import Divider from 'material-ui/Divider';
import { withStyles } from 'material-ui/styles';

const styles = theme => ({
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
});

const SideMenuHeader = ({ classes, children }) => (
  <div>
    <div className={classes.drawerHeader}>
      {children}
    </div>
    <Divider />
  </div>
);

SideMenuHeader.propTypes = {
  classes: PropTypes.shape({
    drawerHeader: PropTypes.string,
  }).isRequired,
};

export default withStyles(styles)(SideMenuHeader);
