import { connect } from 'react-redux';
import SideMenuToggle from 'components/dashboard/side-menu/toggle-button';
import actions from 'api/actions';

const mapDispatchToProps = dispatch => ({
  toggleSideMenu: () => dispatch({
    type: actions.TOGGLE_SIDE_MENU,
  }),
});

export default connect(null, mapDispatchToProps)(SideMenuToggle);
