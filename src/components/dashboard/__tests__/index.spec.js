import React from 'react';
import { createShallow } from 'material-ui/test-utils';

import Dashboard from '../index';

describe('Dashboard', () => {
  const shallow = createShallow({ dive: true });
  const dashboard = shallow(<Dashboard>Hello World!</Dashboard>);

  it('renders properly', () => {
    expect(dashboard).toMatchSnapshot();
  });

  it('should render a Dashboard with child', () => {
    expect(dashboard.childAt(0).contains('Hello World!'));
  });
});
