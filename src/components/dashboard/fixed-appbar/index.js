import React from 'react';
import PropTypes from 'prop-types';
import AppBar from 'material-ui/AppBar';
import { withStyles } from 'material-ui/styles';

const styles = () => ({
  root: {
    width: '100%',
    height: '100%',
    position: 'relative',
    display: 'flex',
  },
  appBar: {
    position: 'fixed',
    top: '0',
    left: 'auto',
    right: 'auto',
  },
});

const FixedAppbar = ({
  children,
  classes,
}) => (
  <AppBar
    className={classes.appBar}
    position="fixed"
  >
    {children}
  </AppBar>
);

FixedAppbar.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  classes: PropTypes.shape({
    root: PropTypes.string,
    appBar: PropTypes.string,
  }).isRequired,
};

export default withStyles(styles)(FixedAppbar);
