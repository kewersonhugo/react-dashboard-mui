import React from 'react';
import PropTypes from 'prop-types';
import MuiToolbar from 'material-ui/Toolbar';

const Toolbar = ({ children }) => (
  <MuiToolbar disableGutters>
    {children}
  </MuiToolbar>
);

Toolbar.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default Toolbar;
