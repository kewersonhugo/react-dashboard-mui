import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import classNames from 'classnames';

const drawerWidth = 240;

const styles = () => ({
  menuOpened: {
    '&$root': {
      marginLeft: `${drawerWidth}px`,
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  root: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
});

class Dashboard extends React.PureComponent {
  render() {
    const {
      classes,
      children,
      open,
    } = this.props;
    return (
      <div className={classNames(classes.root, open && classes.menuOpened)}>
        {children}
      </div>
    );
  }
}

Dashboard.defaultProps = {
  children: null,
  open: false,
};

Dashboard.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  classes: PropTypes.shape({
    root: PropTypes.string,
    appBar: PropTypes.string,
  }).isRequired,
  open: PropTypes.bool,
};

export default withStyles(styles)(Dashboard);
