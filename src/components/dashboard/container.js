import { connect } from 'react-redux';
import Dashboard from './index';

const mapStateToProps = state => ({
  open: state.get('dashboard').sideMenu.open,
});

export default connect(mapStateToProps)(Dashboard);

