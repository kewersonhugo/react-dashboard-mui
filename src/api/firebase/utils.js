const parseFirebaseAuthError = (error = {}) => {
  switch (error.code) {
    case 'auth/weak-password':
      return {
        field: 'PASSWORD',
        message: 'Sua senha deve ter no mínimo 6 caracteres',
      };
    case 'auth/wrong-password':
      return {
        field: 'PASSWORD',
        message: 'Senha incorreta',
      };
    case 'auth/user-not-found':
      return {
        field: 'EMAIL',
        message: 'O usuário não existe ou foi deletado',
      };
    case 'auth/invalid-email':
      return {
        field: 'EMAIL',
        message: 'Insira um endereço de e-mail válido',
      };
    case 'auth/email-already-in-use':
      return {
        field: 'EMAIL',
        message: 'Este e-mail já está cadastrado',
      };
    default:
      return {};
  }
};

export default parseFirebaseAuthError;
