import { put, takeLatest } from 'redux-saga/effects';
import firebase from 'api/firebase';
import actions from 'api/actions';
import { push } from 'react-router-redux';

function* registerNewUser(action) {
  const { email, password } = action.payload;
  try {
    const response = yield firebase.auth().createUserWithEmailAndPassword(email, password);
    console.log(response);
    yield put({ type: actions.REGISTER_NEW_USER_SUCCESSFUL });
    yield put(push('/'));
  } catch (err) {
    console.log(err);
    yield put({ type: actions.REGISTER_NEW_USER_FAILED, payload: err });
  }
}

export default function* watchRegisterNewUser() {
  yield takeLatest(actions.REGISTER_NEW_USER, registerNewUser);
}
