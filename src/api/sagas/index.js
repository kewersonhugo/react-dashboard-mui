import { all } from 'redux-saga/effects';
import watchRegisterNewUser from 'api/sagas/watchRegisterNewUser';
import watchLoginUser from 'api/sagas/watchLoginUser';
import watchLogoutUser from 'api/sagas/watchLogoutUser';
import watchRecoverUserPassword from 'api/sagas/watchRecoverUserPassword';

export default function* rootSaga() {
  yield all([
    watchRegisterNewUser(),
    watchLoginUser(),
    watchLogoutUser(),
    watchRecoverUserPassword(),
  ]);
}
