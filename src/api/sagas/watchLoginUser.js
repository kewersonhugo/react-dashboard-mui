import { all, put, takeLatest } from 'redux-saga/effects';
import firebaseApp from 'api/firebase';
import GoogleFirebase from 'firebase';
import actions from 'api/actions';
import { push } from 'react-router-redux';

function* loginUser(
  firebase, action, signInFunction,
) {
  try {
    const { user } = yield signInFunction();
    const {
      displayName, photoURL, email, uid,
    } = user;
    const ref = yield firebase.database().ref(`/users/${uid}`);
    yield ref.update({ displayName, photoURL, email });
    yield put({ type: actions.LOGIN_USER_SUCCESSFUL });
    yield put(push('/'));
  } catch (err) {
    console.log(err);
    yield put({ type: actions.LOGIN_USER_FAILED, payload: err });
  }
}

const loginWithEmailAndPassword = (firebase, action) => {
  const { email, password } = action.payload;
  const signInWithEmailAndPassword = () => firebase
    .auth()
    .signInAndRetrieveDataWithEmailAndPassword(email, password);

  return loginUser(
    firebase, action, signInWithEmailAndPassword,
  );
};

const loginWithFacebook = (firebase, action) => {
  const signInWithFacebook = () => firebase
    .auth()
    .signInWithPopup(new GoogleFirebase.auth.FacebookAuthProvider());

  return loginUser(
    firebase, action, signInWithFacebook,
  );
};

const loginWithGoogle = (firebase, action) => {
  const authProvider = new GoogleFirebase.auth.GoogleAuthProvider();
  authProvider.addScope('profile');
  authProvider.addScope('email');
  authProvider.addScope('https://www.googleapis.com/auth/contacts.readonly');
  const signInWithGoogle = () => firebase
    .auth()
    .signInWithPopup(authProvider);

  return loginUser(
    firebase, action, signInWithGoogle,
  );
};

export default function* watchLoginUser() {
  yield all([
    takeLatest(
      actions.LOGIN_USER, loginWithEmailAndPassword, firebaseApp,
    ),
    takeLatest(
      actions.LOGIN_USER_WITH_FACEBOOK, loginWithFacebook, firebaseApp,
    ),
    takeLatest(
      actions.LOGIN_USER_WITH_GOOGLE, loginWithGoogle, firebaseApp,
    ),
  ]);
}
