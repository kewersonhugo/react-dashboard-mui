import { put, takeLatest } from 'redux-saga/effects';
import firebase from 'api/firebase';
import actions from 'api/actions';
import { push } from 'react-router-redux';

function* logoutUser() {
  try {
    const response = yield firebase.auth().signOut();
    console.log(response);
    yield put(push('/login'));
  } catch (err) {
    console.log(err);
  }
}


export default function* watchLogoutUser() {
  yield takeLatest(actions.LOGOUT_USER, logoutUser);
}
