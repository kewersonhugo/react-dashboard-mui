import { put, takeLatest } from 'redux-saga/effects';
import firebase from 'api/firebase';
import actions from 'api/actions';
import { push } from 'react-router-redux';

function* recoverUserPassword(action) {
  const { email } = action.payload;
  try {
    const response = yield firebase.auth().sendPasswordResetEmail(email);
    console.log(response);
    yield put({ type: actions.RECOVER_USER_PASSWORD_SUCCESSFUL });
    yield put(push('/login'));
  } catch (err) {
    console.log(err);
    yield put({ type: actions.RECOVER_USER_PASSWORD_FAILED, payload: err });
  }
}


export default function* watchRecoverUserPassword() {
  yield takeLatest(actions.RECOVER_USER_PASSWORD, recoverUserPassword);
}
