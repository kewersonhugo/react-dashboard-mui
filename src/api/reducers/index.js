import router from 'api/reducers/router';
import dashboard from 'api/reducers/dashboard';
import user from 'api/reducers/user';

export default {
  router,
  dashboard,
  user,
};
