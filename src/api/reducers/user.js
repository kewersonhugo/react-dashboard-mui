import actions from 'api/actions';

export default (state = {}, action) => {
  if (action.type === actions.UPDATED_FORM) {
    return {
      ...state,
      ...action.payload,
    };
  } else if (action.type === actions.LOGIN_USER_SUCCESSFUL) {
    return {
      state: {},
    };
  } else if (action.type === actions.LOGIN_USER_FAILED) {
    return {
      ...state,
      error: action.payload,
    };
  } else if (action.type === actions.RECOVER_USER_PASSWORD_SUCCESSFUL) {
    return {
      ...state,
      error: action.payload,
    };
  } else if (action.type === actions.RECOVER_USER_PASSWORD_FAILED) {
    return {
      ...state,
      error: action.payload,
    };
  } else if (action.type === actions.REGISTER_NEW_USER_SUCCESSFUL) {
    return {
      ...state,
      error: action.payload,
    };
  } else if (action.type === actions.REGISTER_NEW_USER_FAILED) {
    return {
      ...state,
      error: action.payload,
    };
  }
  return state;
};
