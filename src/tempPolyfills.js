global.requestAnimationFrame = (callback) => {
  setTimeout(callback, 0);
};

const { requestAnimationFrame } = global;

export default requestAnimationFrame;
