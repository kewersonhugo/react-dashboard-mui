const path = require('path');
const webpack = require('webpack');

const LIBRARY_NAME = 'react-dashboard-mui'
console.warn(process.env.REACT_APP_FIREBASE_API_KEY);

module.exports = {
  entry: {
    'Api': ['./src/api'],
    'Components': ['./src/components'],
  },
  resolve: {
    modules: [path.resolve(__dirname, 'src'), 'node_modules'],
  },
  output: {
    path: path.join(__dirname, 'lib'),
    filename: '[name].js',
    library: LIBRARY_NAME,
    libraryTarget: 'umd',
    umdNamedDefine: true,
  },
  externals: [
    function(context, request, callback) {
      if (/^(material-ui|firebase|react|react-dom|react-redux|redux-saga|immutable|classnames|prop-types|recompose)/.test(request)){
        return callback(null, 'commonjs ' + request);
      }
      callback();
    },
  ],
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'REACT_APP_FIREBASE_API_KEY': 'process.env.REACT_APP_FIREBASE_API_KEY',
        'REACT_APP_FIREBASE_AUTH_DOMAIN': 'process.env.REACT_APP_FIREBASE_AUTH_DOMAIN',
        'REACT_APP_FIREBASE_DATABASE_URL': 'process.env.REACT_APP_FIREBASE_DATABASE_URL',
        'REACT_APP_FIREBASE_PROJECT_ID': 'process.env.REACT_APP_FIREBASE_PROJECT_ID',
        'REACT_APP_FIREBASE_STORAGE_BUCKET': 'process.env.REACT_APP_FIREBASE_STORAGE_BUCKET',
        'REACT_APP_FIREBASE_SENDER_ID': 'process.env.REACT_APP_FIREBASE_SENDER_ID',
      }
    })
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['react', 'es2015', 'stage-0'],
            plugins: ['transform-runtime'],
          },
        }
      }
    ]
  }
};